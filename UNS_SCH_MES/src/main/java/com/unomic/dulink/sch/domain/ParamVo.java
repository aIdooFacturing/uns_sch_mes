package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class ParamVo{
	String dvcId;
	String date;
	String dateTime;
	String stDate;
	String edDate;
	String tgDate;
}
