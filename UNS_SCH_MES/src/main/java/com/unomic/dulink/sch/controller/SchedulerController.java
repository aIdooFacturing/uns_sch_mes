package com.unomic.dulink.sch.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.ParamVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;

	@RequestMapping(value="spTest")
	@ResponseBody
	public void spTest(){
		//DeviceVo dvcVo = new DeviceVo();
		//dvcVo.setWorkDate("2015-10-06");
		//deviceService.calcDeviceTimesTest(dvcVo);
		//deviceService.calcDeviceTimes(dvcVo);
		//calcDeviceTimes();
	}
	@RequestMapping(value="syncMesTest")
	@ResponseBody
	public String syncMesTest(ParamVo prmVo){
		prmVo.setDate("2017-06-30");
		try{
			schService.addDvcTg(prmVo);
			schService.addDfct(prmVo);
			schService.editDvcSmry(prmVo);
		}catch(Exception e){
			LOGGER.error("SQLERR?:"+e.toString());
		}
		
		return "OK";
	}
	
	
	@RequestMapping(value="addDvcSmryTest")
	@ResponseBody
	public String addDvcSmryTest(ParamVo prmVo){
		prmVo.setDate("2017-06-23");
		schService.addDvcSmry(prmVo);
		
		return "OK";
	}
	
	@RequestMapping(value="addDvcLastTest")
	@ResponseBody
	public String addDvcLastTest(){
		schService.addDvcLast();
		return "OK";
	}
	
	
	@RequestMapping(value="editDvcLastTest")
	@ResponseBody
	public String editDvcLastTest(){
		schService.editDvcLast();
		return "OK";
	}
	
	@RequestMapping(value="editDvcLastStatusTest")
	@ResponseBody
	public String editDvcLastStatusTest(){
		schService.editDvcLastStatus();
		return "OK";
	}
	
	
	/*
	 * st
	 * */
	@RequestMapping(value="testSyncMes")
	@ResponseBody
	public String testSyncMes(String tgDate){
		LOGGER.error("RUN testSyncMes");
		
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		//LOGGER.error("strDate:"+strDate);
		
		prmVo.setDate(tgDate);
		try{
			schService.addDvcTg(prmVo);
			schService.addDfct(prmVo);
			schService.editDvcSmry(prmVo);
		}catch(Exception e){
			LOGGER.error("SQLERR?:"+e.toString());
		}
		return "OK";
	}
	
	@Scheduled(cron="5 10 * * * * ")
	public void cronSyncMes(){
		LOGGER.error("RUN cronSyncMes");
		
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		String strDate = CommonFunction.getYstDay();
		LOGGER.error("strDate:"+strDate);
		prmVo.setDate(strDate);
		try{
			schService.addDvcTg(prmVo);
			schService.addDfct(prmVo);
			schService.editDvcSmry(prmVo);
		}catch(Exception e){
			LOGGER.error("SQLERR?:"+e.toString());
		}
		
	}
	
	@RequestMapping(value="testYstDay")
	@ResponseBody
	public String testYstDay(){
		
		return CommonFunction.getYstDay();
	}
	
	
	//매시 10분 5초에 실행.
	//@Scheduled(cron="5 10 * * * * ")
	public void cronTest(){
			LOGGER.info("!!cronTesStart:"+CommonFunction.getTodayDateTime());
		
	}
	
	@RequestMapping(value="addTmChtTest")
	@ResponseBody
	public String addTmChtTest(){
		ParamVo prmVo = new ParamVo();
		
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTIme:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		
		schService.addDvcTmChart(prmVo);
		return "OK";
	}
	
	//@Scheduled(fixedDelay = 10000)
	public void schEditDvcLast(){
		schService.editDvcLastStatus();
		schService.editDvcLast();
	}

	//@Scheduled(fixedDelay = 10000)
	public void schAddDvcSmry(){
		ParamVo prmVo = new ParamVo();
		//addTimeChart();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		
		schService.addDvcSmry(prmVo);
		
		LOGGER.info("run schAddDvcSmry.");
	}
	
	//@Scheduled(fixedDelay = 60000)
	public void schAddTmCht(){
		ParamVo prmVo = new ParamVo();
		
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTIme:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		
		schService.addDvcTmChart(prmVo);
		LOGGER.info("run schAddTmCht.");
	}

	
	public void schAddDvcAll(){
		
	}
	
	public void addDvcSummary(){
		
	}
	@RequestMapping(value="batchAddDvcSmry")
	@ResponseBody
	public String batchTimeChart(ParamVo inputVo){
		LOGGER.info("stDate:"+inputVo.getStDate());
		LOGGER.info("edDate:"+inputVo.getEdDate());
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		LOGGER.info("inputVo.getStDate():"+inputVo.getStDate());
		LOGGER.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
			ParamVo prmVo = new ParamVo();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	       //DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.zzz");
	       dateFormat.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
           Date dateSt = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());
           LOGGER.info("tgDate:"+tgDate);

           inputVo.setTgDate(tgDate);
           
           LOGGER.info("getTgDate:"+inputVo.getTgDate());
           LOGGER.info("edDate:"+edDate);
           

           //CommonFunction.dateTime2Mil(tgDate)
           //while(!tgDate.equals(edDate)){
           //while(CommonFunction.dateTime2Mil(tgDate) < (CommonFunction.dateTime2Mil(edDate))){
           while(!tgDate.equals(edDate)){
        	   LOGGER.info("@@@@@@@@@@@@@@@@@@ING@@@@@@@@@@@@@@@@@@");
        	   //deviceService.addTimeChart(inputVo);
        	   prmVo.setDate(inputVo.getTgDate());
        	   
        	   schService.addDvcSmry(prmVo);

        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTgDate(tgDate);
               LOGGER.info("tgDate:"+tgDate);
           }
           // EOL
           LOGGER.info("@@@@@@@@@@@@@@@@@@EOL@@@@@@@@@@@@@@@@@@");
           LOGGER.info("edDate:"+tgDate);
           LOGGER.info("edDate:"+edDate);
 
		return "OK";
	}
	
}

