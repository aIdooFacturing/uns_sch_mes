package com.unomic.dulink.sch.service;

import com.unomic.dulink.sch.domain.ParamVo;

public interface SchService {
	public String addDvcTg(ParamVo prmVo);
	public String addDfct(ParamVo prmVo);
	public String editDvcSmry(ParamVo prmVo);
	public String addDvcSmry(ParamVo prmVo);
	public String addDvcLast();
	public String editDvcLast();
	public String editDvcLastStatus();
	public String addDvcTmChart(ParamVo prmVo);
}
