package com.unomic.dulink.sch.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.ParamVo;

@Service
@Repository
public class SchServiceImpl implements SchService{

	private final static String SCH_SPACE= "com.unos.sch.";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SchServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	
//	<insert id='addDvcTg' parameterType="paramVo">
//	<update id='editDvcSmry' parameterType="paramVo">
//	<insert id='addDfct' parameterType="paramVo">
	@Override
	@Transactional//(value="txManager_ma")
	public String addDvcTg(ParamVo prmVo)
	{
		LOGGER.info("addDvcTg");
		LOGGER.info("prmVo.getDate():"+prmVo.getDate());
		sql_ma.insert(SCH_SPACE + "addDvcTg", prmVo);
		return "OK";
	}
	@Override
	@Transactional//(value="txManager_ma")
	public String addDfct(ParamVo prmVo)
	{
		LOGGER.info("addDfct");
		LOGGER.info("prmVo.getDate():"+prmVo.getDate());
		sql_ma.insert(SCH_SPACE + "addDfct", prmVo);
		return "OK";
	}
	public String editDvcSmry(ParamVo prmVo){
		LOGGER.info("RUN editDvcSmry");
		LOGGER.info("prmVo.getDate():"+prmVo.getDate());
		sql_ma.update(SCH_SPACE+"editDvcSmry",prmVo);
		return "OK";
	}
	
	@Override
	@Transactional//(value="txManager_ma")
	public String addDvcSmry(ParamVo prmVo)
	{
		LOGGER.info("RunAddDvcSmry");
		LOGGER.info("prmVo.getDate():"+prmVo.getDate());
		sql_ma.insert(SCH_SPACE + "addDvcSmry", prmVo);
		return "OK";
	}
	
	@Override
	@Transactional//(value="txManager_ma")
	public String addDvcTmChart(ParamVo prmVo)
	{
		sql_ma.insert(SCH_SPACE + "addDvcTmChart", prmVo);
		return "OK";
	}
	
	public String addDvcLast(){
		sql_ma.insert(SCH_SPACE+"addDvcLast");
		return "OK";
	}
	
	public String editDvcLast(){
		LOGGER.info("RUN editDvcLast");
		sql_ma.update(SCH_SPACE+"editDvcLast");
		return "OK";
	}
	
	public String editDvcLastStatus(){
		LOGGER.info("RUN editDvcLastStatus");
		sql_ma.update(SCH_SPACE+"editDvcLastStatus");
		return "OK";
	}

	
	
	
}