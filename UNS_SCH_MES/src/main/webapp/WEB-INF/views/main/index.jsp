<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<html>
<head>
<title>Home</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" /> 
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/login/index.js"></script>
<link rel="stylesheet" href="${ctxPath}/css/login/index.css">
</head>
<body>
	<div id="corver"></div>
	<video id="bgvid" loop muted autoplay="autoplay">
		<source src="http://106.240.234.114:8080/securobot/01.m4v" type="video/mp4">
	</video>
	
	<div id="loginForm">
		<form id="login" onsubmit="return false;">
			<Center>
				<img src="${ctxPath }/images/loginForm/SmartFactory.png" id="logo">
				<img src="${ctxPath }/images/loginForm/Factory911_2.png" id="logo2">
				<input type="email" placeholder="E-mail" id="email" data-role="none" >
				<input type="password" placeholder="Password" id="pwd" data-role="none"><br>
				<img alt="" src="${ctxPath }/images/KOMAF.jpg" width="20%"><br>
				<a href="${ctxPath }/chart/chart_real.do" rel="external">Enter 2015 KOMAF </a>
				<br><br>
				<table>
					<tr>
						<td valign="top"><font style="font-size: 15" id="saveIdText">Keep me signed in</font></td>
						<td valign="top"><input type="checkbox" id="saveId"></td>
					</tr>
				</table>
				<hr id="hr">
				<font id="joinBtn1" >Don't have a factory911 ID? </font> <u><b><font id="joinBtn" >Create one now.</font></b></u>
				<br>
				<div id="errMsg" class="errMsg"></div>
			</Center>
		</form>
	</div>
	
	<div id="joinForm">
		<form id="join" onsubmit="return false;">
			<img alt="" src="${ctxPath }/images/joinForm/arrow_left.png" id="backArrow">
			<Center>
				<h2>Factory 911</h2>
				<input type="email" placeholder="E-mail" id="email" data-role="none">
				<input type="text" placeholder="Company" id="company" data-role="none">
				<input type="password" placeholder="Password" id="pwd" data-role="none">
				<input type="password" placeholder="Confirm Password" id="confirmPwd" data-role="none">
				
				<div id="NCList">
				</div>
				
				<div id="errMsg" class="errMsg"></div>
				<button data-inline="true" id="joinBtn" >Join</button>
				<button data-inline="true" id="resetBtn" >Reset</button>
			</Center>
		</form>
	</div>
</body>
</html>
